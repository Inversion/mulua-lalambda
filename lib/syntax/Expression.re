open Base;

module Literal = {
  [@deriving sexp]
  type t =
    | Nil
    | Boolean(bool)
    | Integer(int);
};

module rec Expression: {
  [@deriving sexp]
  type t =
    | Identifier(Identifier.t)
    | Literal(Literal.t)
    | UnaryExpression(UnaryExpression.t)
    | BinaryExpression(BinaryExpression.t)
    | Call(Call.t);
} = Expression

and UnaryExpression: {
  module UnaryOperator: {
    [@deriving sexp]
    type t =
      | Negate;
  };

  [@deriving sexp]
  type t = {
    operator: UnaryOperator.t,
    expression: Expression.t,
  };
} = UnaryExpression

and BinaryExpression: {
  module BinaryOperator: {
    [@deriving sexp]
    type t =
      | Add
      | Subtract
      | Multiply
      | DivideInteger
      | Equal
      | NotEqual
      | Less
      | LessOrEqual
      | Greater
      | GreaterOrEqual;
  };

  [@deriving sexp]
  type t = {
    left: Expression.t,
    operator: BinaryOperator.t,
    right: Expression.t,
  };
} = BinaryExpression

and Call: {
  [@deriving sexp]
  type t = {
    callee: Identifier.t,
    arguments: list(Expression.t),
  };
} = Call;

include Expression;
