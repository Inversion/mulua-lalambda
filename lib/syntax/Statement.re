open Base;

module rec Statement: {
  [@deriving sexp]
  type t =
    | Assignment(Assignment.t)
    | If(If.t)
    | While(While.t)
    | Expression(Expression.t);
} = Statement

and Block: {
  [@deriving sexp]
  type t = list(Statement.t);
} = Block

and Assignment: {
  module AssignmentOperator: {
    [@deriving sexp]
    type t =
      | Assign;
  };

  [@deriving sexp]
  type t = {
    left: Identifier.t,
    operator: AssignmentOperator.t,
    right: Expression.t,
  };
} = Assignment

and If: {
  [@deriving sexp]
  type t = {
    test: Expression.t,
    consequent: Block.t,
    alternative: option(Block.t),
  };
} = If

and While: {
  [@deriving sexp]
  type t = {
    test: Expression.t,
    body: Block.t,
  };
} = While;

include Statement;
