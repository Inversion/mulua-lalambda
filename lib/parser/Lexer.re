exception Unexpected(string);

let letter = [%sedlex.regexp? 'a' .. 'z' | 'A' .. 'Z'];
let digit = [%sedlex.regexp? '0' .. '9'];
let integer = [%sedlex.regexp? Plus(digit)];

let identifier_start = [%sedlex.regexp? letter | '_'];
let identifier_continue = [%sedlex.regexp? identifier_start | digit];
let identifier = [%sedlex.regexp?
  (identifier_start, Star(identifier_continue))
];

let rec token = lexbuf => {
  open Tokens;
  open Sedlexing.Utf8;
  switch%sedlex (lexbuf) {
  | white_space => token(lexbuf)
  | "nil" => NilLiteral
  | "true" => true |> BooleanLiteral
  | "false" => false |> BooleanLiteral
  | "end" => End
  | "if" => If
  | "then" => Then
  | "else" => Else
  | "while" => While
  | "do" => Do
  | "(" => ParenLeft
  | ")" => ParenRight
  | "=" => Equal
  | "+" => Plus
  | "-" => Minus
  | "*" => Star
  | "//" => DoubleSlash
  | "==" => EqualEqual
  | "~=" => TildeEqual
  | "<=" => LessEqual
  | "<" => Less
  | ">=" => GreaterEqual
  | ">" => Greater
  | integer => lexbuf |> lexeme |> int_of_string |> IntegerLiteral
  | identifier => lexbuf |> lexeme |> Identifier
  | eof => EOF
  | _ => lexbuf |> lexeme |> Unexpected |> raise
  };
};
