%{
open MuLua_syntax
%}

%start <Program.t> program

%%

let program :=
  | ~ = block; EOF;
    <>

let block :=
  | ~ = statement*;
    <>

let statement :=
  | ~ = assignment_statement;
    <>
  | ~ = if_statement;
    <>
  | ~ = while_statement;
    <>
  | ~ = call_expression;
    <Statement.Expression>

let assignment_statement :=
  | left = assignable; operator = assignment_operator; right = expression;
    {
      Statement.Assignment { left; operator; right; }
    }

let assignable ==
  | ~ = Identifier;
    <>

let assignment_operator ==
  | Equal;
    { Statement.Assignment.AssignmentOperator.Assign }

let if_statement :=
  | If; test = expression; Then;
    consequent = block;
    alternative = option(Else; ~ = block; <>);
    End;
    {
      Statement.If { test; consequent; alternative; }
    }

let while_statement :=
  | While; test = expression; Do; body = block; End;
    {
      Statement.While { test; body; }
    }

let expression :=
  | ~ = Identifier;
    <Expression.Identifier>
  | ~ = literal;
    <>
  | ~ = parenthesized_expression;
    <>
  | ~ = unary_expression;
    <>
  | ~ = binary_expression;
    <>
  | ~ = call_expression;
    <>

let literal :=
  | NilLiteral;
    {
      Expression.Literal Expression.Literal.Nil
    }
  | value = BooleanLiteral;
    {
      Expression.Literal (Expression.Literal.Boolean value)
    }
  | value = IntegerLiteral;
    {
      Expression.Literal (Expression.Literal.Integer value)
    }

let parenthesized_expression :=
  | ParenLeft; ~ = expression; ParenRight;
    <>

let unary_expression :=
  | operator = unary_operator; ~ = expression;
    {
      Expression.UnaryExpression { operator; expression; }
    }

let unary_operator ==
  | Minus;
    { Expression.UnaryExpression.UnaryOperator.Negate }

let binary_expression :=
  | left = expression; operator = binary_operator; right = expression;
    {
      Expression.BinaryExpression { left; operator; right; }
    }

let binary_operator ==
  | Plus;
    { Expression.BinaryExpression.BinaryOperator.Add }
  | Minus;
    { Expression.BinaryExpression.BinaryOperator.Subtract }
  | Star;
    { Expression.BinaryExpression.BinaryOperator.Multiply }
  | DoubleSlash;
    { Expression.BinaryExpression.BinaryOperator.DivideInteger }
  | EqualEqual;
    { Expression.BinaryExpression.BinaryOperator.Equal }
  | TildeEqual;
    { Expression.BinaryExpression.BinaryOperator.NotEqual }
  | LessEqual;
    { Expression.BinaryExpression.BinaryOperator.LessOrEqual }
  | GreaterEqual;
    { Expression.BinaryExpression.BinaryOperator.GreaterOrEqual }
  | Less;
    { Expression.BinaryExpression.BinaryOperator.Less }
  | Greater;
    { Expression.BinaryExpression.BinaryOperator.Greater }


let call_expression :=
  | callee = callable; ~ = arguments;
    {
      Expression.Call { callee; arguments; }
    }

let callable ==
  | ~ = assignable;
    <>

let arguments ==
  | ParenLeft; ~ = separated_list(Comma, expression); ParenRight;
    <>
