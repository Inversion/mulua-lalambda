/**
  [TotalMap] is a finitely represented total map, constant almost everywhere.
  Represented by as a default value and a partial [Map] of defined non-default elements.
 */
open Base;

module type S = {
  type key;
  type value;
  type map;

  [@deriving equal]
  type t =
    pri {
      default: value,
      different: map,
    };

  let const: value => t;

  let get: (t, key) => value;
  let set: (t, ~key: key, ~value: value) => t;
};

module Make =
       (Key: Comparable.S, Value: Equal.S)

         : (
           S with type key := Key.t with type value := Value.t with
             type map := Map.M(Key).t(Value.t)
       ) => {
  [@deriving equal]
  type t = {
    default: Value.t,
    different: Map.M(Key).t(Value.t),
  };

  let const = default => {default, different: Map.empty((module Key))};

  let get = ({default, different}, key) =>
    key |> Map.find(different) |> Option.value(~default);

  let set = ({default, different}, ~key, ~value) => {
    default,
    different:
      Value.equal(value, default)
        ? Map.remove(different, key) : Map.set(different, ~key, ~data=value),
  };
};
