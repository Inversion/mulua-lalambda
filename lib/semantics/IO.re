open Base;
open MuLua_syntax;

let print = arguments => {
  arguments
  |> List.map(~f=Value.to_string)
  |> String.concat(~sep=" ")
  |> Stdio.print_endline;
  Value.Nil;
};
let input = _ => Stdlib.Scanf.scanf("%d", Fn.id) |> Value.Integer;

let library =
  Map.of_alist_exn(
    (module Identifier),
    [("print", print), ("input", input)],
  );
