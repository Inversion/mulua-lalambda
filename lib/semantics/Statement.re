open Base;
open MuLua_syntax.Statement;

let rec eval_block = (~env=Environment.empty) =>
  List.fold(~init=env, ~f=env => eval(~env))

and eval = (~env=Environment.empty) => {
  let truthy = Expression.eval(~env) %> Value.is_truthy;
  fun
  | Assignment({left, operator: Assign, right}) =>
    env |> Environment.set(~key=left, ~value=right |> Expression.eval(~env))
  | If({test, consequent, alternative}) =>
    (test |> truthy ? Option.Some(consequent) : alternative)
    |> Option.fold(~init=env, ~f=env => eval_block(~env))
  | While({test, body}) as loop =>
    test |> truthy ? loop |> eval(~env=body |> eval_block(~env)) : env
  | Expression(expression) => {
      expression |> Expression.eval(~env) |> ignore;
      env;
    };
};
