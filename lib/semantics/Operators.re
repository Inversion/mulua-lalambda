open Base;
open Value;

exception ExpectedInteger;

module LiftIntegers = {
  let unary = operator =>
    fun
    | Integer(x) => x |> operator |> Integer
    | _ => ExpectedInteger |> raise;

  let binary = (operator, x, y) =>
    switch (x, y) {
    | (Integer(x), Integer(y)) => operator(x, y) |> Integer
    | _ => ExpectedInteger |> raise
    };
};

module LiftIntegerComparison = {
  let binary = (compare, x, y) =>
    switch (x, y) {
    | (Integer(x), Integer(y)) => compare(x, y) |> Boolean
    | _ => ExpectedInteger |> raise
    };
};

let (~-) = (~-) |> LiftIntegers.unary;

let (+) = (+) |> LiftIntegers.binary;
let (-) = (-) |> LiftIntegers.binary;
let ( * ) = ( * ) |> LiftIntegers.binary;
let (/) = (/) |> LiftIntegers.binary;

let (==) = (x, y) => Value.equal(x, y) |> Boolean;
let (!=) = (x, y) => !Value.equal(x, y) |> Boolean;

let (<) = (<) |> LiftIntegerComparison.binary;
let (>) = (>) |> LiftIntegerComparison.binary;
let (<=) = (<=) |> LiftIntegerComparison.binary;
let (>=) = (>=) |> LiftIntegerComparison.binary;
