open Base;
open MuLua_syntax.Expression;

module Literal = {
  let eval: Literal.t => Value.t =
    fun
    | Nil => Nil
    | Boolean(x) => Boolean(x)
    | Integer(x) => Integer(x);
};

module UnaryExpression = {
  open UnaryExpression;

  module UnaryOperator = {
    open Operators;
    open UnaryOperator;

    let eval = (Negate) => (~-);
  };
};

module BinaryExpression = {
  open BinaryExpression;

  module BinaryOperator = {
    open Operators;
    open BinaryOperator;

    let eval =
      fun
      | Add => (+)
      | Subtract => (-)
      | Multiply => ( * )
      | DivideInteger => (/)
      | Equal => (==)
      | NotEqual => (!=)
      | Less => (<)
      | LessOrEqual => (<=)
      | Greater => (>)
      | GreaterOrEqual => (>=);
  };
};

exception UndefinedFunction(string);

let rec eval = (~env=Environment.empty) =>
  fun
  | Literal(x) => x |> Literal.eval
  | Identifier(x) => x |> Environment.get(env)
  | UnaryExpression({operator, expression}) => {
      let operator = operator |> UnaryExpression.UnaryOperator.eval;
      expression |> eval(~env) |> operator;
    }
  | BinaryExpression({left, operator, right}) => {
      let operator = operator |> BinaryExpression.BinaryOperator.eval;
      operator(left |> eval(~env), right |> eval(~env));
    }
  | Call({callee, arguments}) => {
      let callee =
        switch (callee |> Map.find(IO.library)) {
        | Some(callee) => callee
        | None => callee |> UndefinedFunction |> raise
        };
      let arguments = arguments |> List.map(~f=eval(~env));
      arguments |> callee;
    };
